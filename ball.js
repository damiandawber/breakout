Breakout.ball = function() {
  	var o = {
    	position: {},
		width: (Breakout.canvasWidth / Breakout.numCols) / 5,
		height: (Breakout.canvasWidth / Breakout.numCols) / 5,
		fillStyle: 'rgb(255, 255, 255)',
		active: false,
		velocity: {
        	x: 1,
			y: 2
		},

      	draw: function() {
			var that = this;

			if(typeof Breakout.context !== 'undefined') {
				if(that.active) {
                	that.position.x += that.velocity.x;
					that.position.y -= that.velocity.y;
				}
				Breakout.context.fillStyle = that.fillStyle;
				Breakout.context.fillRect(that.position.x, that.position.y,
					that.width, that.height);
			} 
		},
	};
	return o;
}();
