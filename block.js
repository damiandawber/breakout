/**
 * The default block
 */
Breakout.block = function() {
	var o  = {
		fillStyle: 'rgb(100, 100, 100)',
		position: {
			x:null, 
			y:null
		},
		width: null,
		height: null,
		isHit: false,

		/**
		 * Draw the block on the page
		 */
      	draw: function() {
			var that = this;

			if(typeof Breakout.context !== 'undefined') {
				Breakout.context.fillStyle = that.fillStyle;
				Breakout.context.fillRect(that.position.x, that.position.y,
					that.width, that.height);
			} 
		},
		hit: function() {
			var that = this;
			that.isHit = true;
		}
	};
	return o;
}();
