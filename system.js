// Add a create method to the Object function for ease
// of generating object instances

// Every object is linked to a prototype from which it can
// inherit properties - we create a completely new object
// from an old object, the new object having the properties
// of the old object. @see The Good Parts ch.3

if(typeof Object.create !== 'function') {
	Object.create = function(o) {
    	var F = function() {};
		F.prototype = o;
		return new F();
	}
}
