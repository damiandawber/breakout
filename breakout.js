var Breakout = {
	// The 2D canvas context
	context: undefined,

	// Some properties
	canvasElmId: 'breakout',
	canvasWidth: 400,
	canvasHeight: 250,
	numRows: 8,
	numCols: 10,
	canvasPadding: {tp: 30, rght: 0, btm: 30, lft: 0},
};



