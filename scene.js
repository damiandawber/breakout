Breakout.scene = function() {
	var level = 1;
	var score = 0;

	var blocks = [];
	var paddle;
	var ball;

	/**
	 * Set interval
	 */
	var startInterval = function startInterval() {
			Breakout.interval = window.setInterval(function() {
				draw();	
			}, 17);
	}

	/**
	 * Stop interval
	 */
	var stopInterval = function stopInterval() {
    	window.clearInterval(Breakout.interval);
	}

	/**
	 * Render the scene
	 */
	var draw = function draw() {
		//
		// Display score
		//
		Breakout.context.clearRect(0, 0, Breakout.canvasWidth, Breakout.canvasHeight);
		Breakout.context.textAlign = 'center';
		Breakout.context.strokeStyle = 'white';
		Breakout.context.font = '11pt Arial';
		Breakout.context.strokeText(score, Breakout.canvasWidth / 2 , 18);

		//
		// Draw blocks
		//
		var blockWidth = parseInt(Breakout.canvasWidth / Breakout.numCols, 10);
		var blockHeight = parseInt((Breakout.canvasHeight / 4) / Breakout.numRows, 10);
		
		for(var i = 0; i < Breakout.numRows; i+=1) {
			if(!blocks[i]) {
            	blocks[i] = [];
			}
			for(var j = 0; j < Breakout.numCols; j+=1) {
				var x = blockWidth * j;
				var y = Breakout.canvasPadding.tp + blockHeight * i;

				if(!blocks[i][j]) {
					blocks[i][j] = Object.create(Breakout.block);
				}

				var rgb;
				if(i < 2) {
                  	rgb = 'rgb(212, 212, 212)';
				} else if(i >= 2 && i < 4) {
                  	rgb = 'rgb(156, 156, 156)';
				} else if (i >= 4 && i < 6) {
                  	rgb = 'rgb(115, 115, 115)';
				} else if (i >= 6 && i < 8) {
                  	rgb = 'rgb(81, 81, 81)';
				}
				blocks[i][j].fillStyle = rgb;
				blocks[i][j].position = {x: x, y: y};
				blocks[i][j].width = blockWidth;
				blocks[i][j].height = blockHeight;

				if(!blocks[i][j].isHit) {
					blocks[i][j].draw(x, y, blockWidth, blockHeight);
				}
			}
		}

		//
		// Draw paddle
		//
		paddle = Object.create(Breakout.paddle);
		paddle.width = blockWidth * 2;
		paddle.height = blockHeight;
		paddle.draw();

		//
		// Draw ball
		//
		if(typeof ball === 'undefined') {
			ball = Object.create(Breakout.ball);
		}
		if(!ball.active) {
			ball.position.x = paddle.position.x + 0.5 * (paddle.width - ball.width);
			ball.position.y = paddle.position.y - ball.height - 10; 
		} 
		ball.draw();

		//
		// Handle collisions
		//
		var ballX = ball.position.x,
			ballY = ball.position.y,
			ballX2 = ball.position.x + ball.width,
			ballY2 = ball.position.y + ball.height,
			paddleX = paddle.position.x,
			paddleY = paddle.position.y;

		// Ball-screen collisions
		if(ballX + ball.width > Breakout.canvasWidth || ballX < 0) {
          	ball.velocity.x = -ball.velocity.x;
		}
		if(ballY <= 0) {
        	ball.velocity.y = -ball.velocity.y;
		}
		
		// Ball-block collisions
		for(i = 0, ii = blocks.length; i < ii; i+=1) {
			for(j = 0, jj = blocks[i].length; j < jj; j+=1) { 
				if(!blocks[i][j].isHit) 
				{
					var curBlockX = blocks[i][j].position.x; 
                   	if((ballX > curBlockX 
							&& ballX < curBlockX + blocks[i][j].width)
						|| (ballX2 > curBlockX
							&& ballX2 < curBlockX + blocks[i][j].width))
					{
						if(ballY > blocks[i][j].position.y 
							&& ballY < blocks[i][j].position.y + blocks[i][j].height) 
						{
								ball.velocity.y = -ball.velocity.y;
								blocks[i][j].isHit = true;
								score += 1;
						}
                    	
					}
				}
			}
		}

		// Ball-paddle collisions
		var ballBottomY = ballY + ball.height;
		if((ballX + ball.width) > paddleX 
			&& (ballX - ball.width) < paddleX + paddle.width
			&& ballBottomY >= paddleY 
			&& ballBottomY < paddleY + paddle.height
		) {
           	ball.velocity.y = -ball.velocity.y; 

			if(ballX < paddleX + paddle.width / 2) {
            	ball.velocity.x = -Math.abs(ball.velocity.x);
			} else {
            	ball.velocity.x = Math.abs(ball.velocity.x);
			}
		}

		// Life lost
		if(ballBottomY >= Breakout.canvasHeight) {
        	stopInterval();
		}
	};

	return {
		/**
		 * Initialise the scene
		 */
		init: function() {
			var canvas = document.getElementById(Breakout.canvasElmId);
			if(!canvas || !canvas.getContext) {
				alert('Canvas is not supported in this browser or you have not defined the canvas element.');
				return;
			} else {
                canvas.width = Breakout.canvasWidth;
				canvas.height = Breakout.canvasHeight;

				Breakout.context = canvas.getContext('2d');
			}

			// 
			// Render frames
			//
			startInterval();

			// 
			// Handle key events
			//
			document.onkeydown = function(e) {
				if(e.keyCode === 37) {
					if(paddle.position.x > 0) {
						paddle.position.x -= paddle.velocity.x;
					}
				} else if (e.keyCode === 39) {
					if(paddle.position.x + paddle.width < Breakout.canvasWidth) {
						paddle.position.x += paddle.velocity.x;
					}
				} else if (e.keyCode === 32) {
                	ball.active = true;
				}
			};

		},

	};
}();
