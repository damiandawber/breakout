Breakout.paddle = function() {
  	return {
    	position: {
			x: Breakout.canvasWidth / 8,
			y: Breakout.canvasHeight - Breakout.canvasPadding.btm
		},
		fillStyle: 'rgb(255, 255, 255)',
		velocity: {
        	x: 10
		},

      	draw: function() {
			var that = this;

			if(typeof Breakout.context !== 'undefined') {
				Breakout.context.fillStyle = that.fillStyle;
				Breakout.context.fillRect(that.position.x, that.position.y,
					that.width, that.height);
			} 
		},
	}
}();
